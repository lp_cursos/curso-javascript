
//var x = 7; // This is the old way to declare variables

let a; // Uninitializaed variable. Printing this variable will result in an "undefined" value

let x = 7;  // Initialized variable
let y = 3;
let z = x + y;

const v = 10; // We do not intend for this variable to ever change it's value.

// v = 7; // An error will occur if we do this. We said the value of "v" would not be modified and this line does modify it.

console.log('z = ' + z);
console.log('v = ' + v);

// Variable naming rules:
// 1- All variable names must start with either a letter ("abcd"), a dolar sign ($) or an underscore (_).

let b  = 1;
let $c = 2;
let _d = 3

// 2- Variable names can use letters, numbers, dolar signs and/or underscores but no other special characters nor spaces (identifiers cannot have any spaces).

// 3- Existing key words cannot be used as variable names.

// let let = 4; // Wrong
let let2 = 5; // Ok

// 4- Variable names are key-sensitive
let e = 9;
let E = 8;

console.log('e = ' + e + ' E = ' + E); // E and e are different variables

// You may use the lowercaseUppercase for clarity if your variable's name contains more than one word.
// Also, variables may contain all sorts of values.

let stringExample = 'This is a character string';

console.log(stringExample);