// Sometimes we need a function that will be called only once in the application. 
// That's what Function Expressions are for (similar to lambdas in functional programming)
// Function Expressions: function (arg1, arg2, ..., argN) {line1; line2; ...; lineM;}

// You may write and call a function expression and invoke it immediately by surrounding it 
// with (functional expression) and adding an ();

(function(){console.log('Immediately Invoked Function Expression');})();

// For this example the setTimeout() function is utilized.
// setTimeout(function, timeout) waits for "timeout" miliseconds before executing "function"

setTimeout(function () {console.log('Waited 2 seconds before printing this message');} , 2000);

// The following example is a recursive function that increases a counter every time it is executed
// Scince there is no condition for the recursive function calling, the program will run indefenitely
// Use Ctrl-C to stop the execution

//console.log('\n----Third example----\n');

let counter = 0;

function timeout()
{
    setTimeout
    (
        function()  // First argument for setTimeout()
        {
            console.log('This function was called ' + counter++ + ' time/s' );
            timeout();
        } , 2000    // Second argument for setTimeout()
    );
}

timeout();