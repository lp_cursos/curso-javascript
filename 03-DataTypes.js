// Variables itself do not have data types but the the values we can store in them do.

let x = 4;              // This type is "number". It can store possitive and negative velues as well as decimal numbers

// Once you have determined the data type of a variable you may not change it. 
//let x = 'Hello';      // Uncommenting this statement will produce an error

console.log('x type: ' + typeof x);

let y = 'Hello';        // This type is "string"
let z = true;           // This type is "boolean"

console.log('y type: ' + typeof y);
console.log('z type: ' + typeof z);

let u;                  // The type and value of this variable is undefined

console.log('u type: ' + typeof u);
