// Declearing arrays
let a = [4, 8, 15, 16, 23, 42];
let b = ['Lucas', 'David', 'Alice'];

// Printing arrays and array items
console.log('-----Print current values of an array-----\n');
console.log('a[0] = ' + a[0]);  // This will print "4"
console.log('a[1] = ' + a[1]);  // This will print "8"
console.log('Array "a":'); 
console.log(a);     // This will print "[4, 8, 15, 16, 23, 42]"

//  Modifying array items: arrayName[itemIndex] = newValue
console.log('\n-----Modifying array items-----');
a[0] = 7;

console.log('\nModified value a[0] = ' + a[0] + '\n');

// Arrays' type is object
console.log('-----Type of an array-----');
console.log('\nType of "a" = ' + typeof a);
console.log('Type of "b" = ' + typeof b+ '\n');

// Arrays may contain different data types
console.log('-----Elements of different types-----\n');
let c = [1, "Lucas", true];
console.log('Array "c":'); 
console.log(c); 

// Just like a variable without an assigned value, if an element of an 
// array does not exists (it has not been delcared), it will be undefined:
console.log('\n-----Undefined array items-----');
console.log('\nc[3] = ' + c[3]); // c is contains c[0], c[1] and c[2] => c[3] will be undefined

// Determine the number of elements in an array:
console.log('\n-----Array length-----');
console.log('\nThe array "a" has ' + a.length + ' elements');

// Arrays may have empty elements
console.log('\n-----Empty elements-----');
a[10] = 77;
console.log('\nArray "a":'); 
console.log(a)
console.log('\nThe array "a" has ' + a.length + ' elements');

// Creating elements with nothing in them is not safe. Use the push built-in function to the end
// of the array regardless of the number of existing elements in the array
console.log('\n-----Using push() function-----');
b.push('James');
console.log('\nArray "b":'); 
console.log(b)
console.log('The array "b" has ' + b.length + ' elements');

// You may also remove the last item of the array using the pop built-in function
console.log('\n-----Using pop() function-----');
a.pop();
a.pop();
a.pop();
a.pop();
a.pop();
console.log('\nArray "a":'); 
console.log(a)
console.log('The array "a" has ' + a.length + ' elements');