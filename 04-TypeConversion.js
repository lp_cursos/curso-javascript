let a = 5;
let b = '6';
let c = a + b;

console.log('First attempt - Answer: ' + c);            // Type will be "string" and value will be 56 instead of 11
console.log('First attempt - c type: ' + typeof c);     // This is called "coersion" and it means Javascript forces the variable b into a string 
                                                        // in order to use the "+" operator among variables of the same type. If the programmer
                                                        // requires a different behaviour, such behaviour needs to be specified. This can be done as follows:

b = parseInt(b, 10)                     // 10 is for decimal, it might be hexadecimal, octal, binary, etc.
let d = a + b;

console.log('Second attempt - Answer: ' + d);           
console.log('Second attempt - c type: ' + typeof d);

// If the argument of the pasrseInt() function is incorrect, the return value will let us know without throwing an error.
// For example, we can try to use parseInt() with a string which contains letters intead of numbers (this is a problem for
// string to decimal number conversions, hexadecimal will accept some letters) and the reult will be "NaN" (Not a Number).

e = parseInt('bob',10);
console.log('e = ' + e);           
console.log('e type: ' + typeof d); // Type: number

// It is possible to check if a value is a Non number (NaN) or not using the following function:
let f = isNaN('Hi');
console.log(f);                     // The return value will be true since 'Hi' is not a number