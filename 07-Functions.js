// Function declaration:
function myFunction()
{
    console.log('My Function');
}

function myFunction1()
{
    console.log('I am function 1');
}

function myFunction2()
{
    console.log('I am function 2');
}

function myFunction3()
{
    console.log('I am function 3');
}

// Calling the function by it's name
console.log('\n-----Call myFunction-----\n');
myFunction();

// Assign the function to a variable. This way you get a reference to the function. The variable is "pointing" to the funcion
let a = myFunction;
console.log('\n-----Call myFunction via variable "a"-----\n');
a();

console.log('\n-----Use an array of functions in order to call functions by index-----\n');
let b = [myFunction1, myFunction2, myFunction3];
for (let i = 0; i < 3; i++)
{
    b[i]();
}

// Passing arguments
// Function for "Hello World" program:
function helloWorld(name)
{
    console.log('Hello ' + name + '!');
    console.log('\nArgument type:  ' + typeof name + '\n');
}

console.log('\n-----Function for "Hello World"-----\n');
// Type of "name" will be the type of the argument used when calling the function. 
// The same function may be called with several data types.
helloWorld('Lucas');    
helloWorld(150);
helloWorld(true);

// Functions with return values
function myAdder(a, b)
{
    console.log('myAdder was called with the arguments ' + a + ' and ' + b);
    return a + b;
}

console.log('\n-----myAdder function-----\n');
let c = myAdder(1,2);
console.log('\nresult = ' + c);